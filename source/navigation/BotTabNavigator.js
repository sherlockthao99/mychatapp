import React, {useLayoutEffect} from 'react';
import FriendScreen from '../screens/FriendScreen';
import ChatScreen from '../screens/ChatScreen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import 'react-native-gesture-handler';
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";
import SettingScreen from '../screens/SettingScreen';

const Tab = AnimatedTabBarNavigator();

export default function BotTabNavigator(){

    
    return (

        <Tab.Navigator 
          tabBarOptions={{
            activeTintColor: "#2F7C6E",
            inactiveTintColor: "#222222",
          }}
        >
            <Tab.Screen name="Chat" component={ChatScreen}
              options={{
                tabBarIcon: ({focused , color, size}) => (
                  <FontAwesome5 name="comment" size={size ? size : 27} focused={focused} color={focused ? color : "#222222"} />
                )
              }}
            />
            <Tab.Screen name="Friend" component={FriendScreen} 
              options={{
                tabBarIcon: ({focused , color, size}) => (
                  <FontAwesome5 name="user-friends" size={size? size : 27} focused={focused} color={focused ? color : "#222222"} />
                )
              }}
            />
            <Tab.Screen name="Setting" component={SettingScreen} 
              options={{
                tabBarIcon: ({focused , color, size}) => (
                  <FontAwesome5 name="cogs" size={size? size : 27} focused={focused} color={focused ? color : "#222222"} />
                )
              }}
            />
        </Tab.Navigator>
      );

}